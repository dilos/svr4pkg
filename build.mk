CC= gcc
#CFLAGS+= -D__EXTENSIONS__
INSTALL= ginstall

OBJS= $(SRCS:.c=.o)

clean:
	rm -f $(OBJS)
