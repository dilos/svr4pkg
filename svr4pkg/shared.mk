CFLAGS+= -I../../libpkg -I../hdrs -DTEXT_DOMAIN=\"SUNW_OST_OSCMD\"

PKG_LDADD= -L../../libpkg -lpkg
ADM_LDADD= -ladm
IZONE_LDADD= -linstzones
INST_LDADD= ../libinst/libinst.a

all: $(PROG)

include ../../build.mk

$(PROG): $(OBJS)
	$(CC) -o $@ $^ $(LDFLAGS) $(LDADD)

distclean: clean
	rm -f $(PROG)
